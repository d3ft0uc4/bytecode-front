import datetime
import gridfs
from flask_pymongo import PyMongo, MongoClient
from app import app
from application.core import utils
from application.core.exceptions import NotFoundException

mongo = PyMongo(app, config_prefix='MONGO')
db = MongoClient().blog_posts
fs = gridfs.GridFS(db)


def file_to_base64(file):
    prefix = ''
    if file.content_type == 'png':
        prefix = 'data:image/png;base64,'
    elif file.content_type == 'jpg':
        prefix = 'data:image/jpg;base64,'
    return prefix + file.read()


class BlogPosts:
    @staticmethod
    def list(limit=3, page_number=1):
        data = []
        try:
            cursor = mongo.db.posts.find(limit=limit, skip=(page_number - 1) * limit).sort([('created_at', -1)])
        except:
            cursor = []
        for post in cursor:
            post['day'] = post['created_at'].day
            post['month'] = utils.month_mapper[str(post['created_at'].month)]
            id = post['_id']
            if fs.exists(id):
                file_str = file_to_base64(fs.get(id))
                post['img'] = file_str
            else:
                post['img'] = '#'
            if len(post['text']) > 50:
                post['text'] = post['text'][0:49] + '...'
            data.append(post)
        return data

    @staticmethod
    def get(_id):
        post = mongo.db.posts.find_one({"_id": _id})
        if post is None:
            raise NotFoundException
        post['day'] = post['created_at'].day
        post['month'] = utils.month_mapper[str(post['created_at'].month)]
        if fs.exists(_id):
            file_str = file_to_base64(fs.get(_id))
            post['img'] = file_str
        else:
            post['img'] = '#'
        return post

    @staticmethod
    def post(payload):
        guid = utils.guid()
        title = payload['title']
        text = payload['text']
        img = payload['img']
        created_at = datetime.datetime.now()
        data = {'_id': guid, 'title': title, 'text': text, 'created_at': created_at}
        mongo.db.posts.save(data)
        if img is not None:
            if img.filename.endswith('.png'):
                mongo.save_file(guid, img, content_type=img.content_type)
            elif img.filename.endswith('.jpg'):
                mongo.save_file(guid, img, content_type=img.content_type)

    @staticmethod
    def get_img(fname):
        return mongo.send_file(fname)

    @staticmethod
    def delete(id):
        mongo.db.posts.remove({'_id': id})
