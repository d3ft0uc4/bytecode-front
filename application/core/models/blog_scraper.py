from pymongo import MongoClient
import requests
from bs4 import BeautifulSoup

client = MongoClient()
db = client.bytecode

page = requests.get("http://samarasoft.com/category/info/")
soup = BeautifulSoup(page.content, 'html.parser')

articles = soup.find_all('article')

for article in articles:
    link = article.find('a')['href']
    title = article.find('a')['title']
    img = article.find('img')['src']
    more = requests.get(link)
    soup = BeautifulSoup(more.content, 'html.parser')
    short_description = article.find('p').text
    print {'link': link,
           'title': title,
           'img': img,
           'short_description': short_description
           }
    print '\n\n'
