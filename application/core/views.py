from functools import wraps
from application.core.exceptions import NotFoundException
from application.core.models.blog import BlogPosts
from application.core.repository import *
from flask import Blueprint, render_template, redirect, url_for
from flask import request, Response
from jinja2 import TemplateNotFound

mod = Blueprint('core', __name__)


@mod.route('/')
def index():
    context = {'pagenumber': 0, 'posts': BlogPosts.list(3)}
    return render_template('core/index.html', **context)


@mod.route('/clients/')
def clients_list():
    context = {'pagenumber': 3, 'posts': BlogPosts.list(3)}
    return render_template('clients/index.html', **context)


@mod.route('/clients/<client>')
def portfolio_item(client):
    context = {'pagenumber': 3, 'posts': BlogPosts.list(3)}
    try:
        return render_template('clients/{0}.html'.format(client), **context)
    except TemplateNotFound:
        return render_template('404.html'), 404


@mod.route('/products/')
def products_list():
    context = {'pagenumber': 2, 'posts': BlogPosts.list(3)}
    return render_template('products/index.html', **context)


@mod.route('/products/<product>')
def products_item(product):
    context = {'pagenumber': 2, 'posts': BlogPosts.list(3)}
    try:
        return render_template('products/{0}.html'.format(product), **context)
    except TemplateNotFound:
        return render_template('404.html'), 404


@mod.route('/contact')
def contact():
    context = {'pagenumber': 5, 'posts': BlogPosts.list(3)}
    return render_template('contact/index.html', **context)


@mod.route('/services')
def services_idx():
    context = {'pagenumber': 1, 'posts': BlogPosts.list(3)}
    return render_template('services/index.html', **context)


@mod.route('/blog')
def blog():
    context = {'pagenumber': 4, 'posts': BlogPosts.list(10)}
    return render_template('blog/index.html', **context)


@mod.route('/blog/<id>')
def blog_single_post(id):
    try:
        post = BlogPosts.get(id)
        context = {'pagenumber': 4, 'single_post': post, 'posts': BlogPosts.list(10)}
        return render_template('blog/single-post.html', **context)
    except NotFoundException:
        return render_template('404.html'), 404


def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'zhbon' and password == 'zalooopa1!'


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated


@mod.route('/admin/blog', methods=['GET'])
@requires_auth
def blog_admin_list():
    posts = BlogPosts.list()
    return render_template('admin/blog-table.html', posts=posts)


@mod.route('/admin/blog', methods=['POST'])
@requires_auth
def blog_admin_create():
    title = request.form['name']
    text = request.form['message']
    img = request.files['pic']
    if img.filename == '':
        img = None
    BlogPosts.post({'title': title, 'text': text, 'img': img})
    return redirect(url_for('core.blog_admin_list'))


@mod.route('/admin/blog/create', methods=['GET'])
@requires_auth
def blog_admin_create_form():
    return render_template('admin/blog-new.html')


@mod.route('/admin/blog/delete/<id>', methods=['GET'])
@requires_auth
def blog_admin_delete(id):
    BlogPosts.delete(id)
    return redirect(url_for('core.blog_admin_list'))


@mod.route('/get_img/<name>')
def get_img(name):
    return BlogPosts.get_img(name)


@mod.route('/success_ticket')
def success_ticket():
    context = {'posts': BlogPosts.list(3)}
    return render_template('success_ticket.html', **context)


@mod.route('/demo')
def demo():
    context = {'pagenumber': 6, 'posts': BlogPosts.list(3)}
    return render_template('demo.html', **context)
