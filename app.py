import sys

from flask import Flask, render_template

reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__, static_folder='application/static')
app.config["MONGO_DBNAME"] = "blog_posts"


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


from application.core.views import mod as core

app.register_blueprint(core)
